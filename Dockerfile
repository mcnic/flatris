FROM node

RUN mkdir /app
WORKDIR /app

# cashing
COPY package.json /app
RUN yarn install

COPY . /app
RUN yarn install
RUN yarn test
RUN yarn build
CMD yarn start

EXPOSE 3000

